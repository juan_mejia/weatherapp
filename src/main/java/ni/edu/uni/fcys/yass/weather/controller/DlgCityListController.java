/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.fcys.yass.weather.controller;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.DefaultListModel;
import ni.edu.uni.fcys.yass.weather.pojo.City;

/**
 *
 * @author Yasser
 */
public class DlgCityListController {

    public DlgCityListController() {
    }

    public static List<City> cities = new ArrayList();
    
    private List<City> getCityList() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonReader reader = 
                new JsonReader(new FileReader(getClass().getResource("/ni/edu/uni/fcys/yass/city.list.json").getPath()));
        City[] data = gson.fromJson(reader, City[].class);
        cities = Arrays.asList(data);
        return cities;
    }

    public DefaultListModel getListModel() throws FileNotFoundException {
        List<City> lista = new ArrayList<>();
        DefaultListModel listModel = new DefaultListModel();
        getCityList().parallelStream()
                .filter((city) -> city.getCountry().equals("NI"))
                .forEach((city) -> {
            listModel.addElement(city.toString());    
            lista.add(city);
        });
        
        cities = lista;
        return listModel;
    }
    
    public DefaultListModel getListModelByCity(String cityName) throws FileNotFoundException {
        List<City> lista = new ArrayList<>();
        DefaultListModel listModel = new DefaultListModel();
        getCityList().parallelStream()
                .filter((city) -> city.getCountry().equals("NI"))
                .filter((city) -> city.getName().toLowerCase().contains(cityName.toLowerCase()))
                .forEach((city) -> {
            listModel.addElement(city.toString());   
            lista.add(city);
        });
        cities = lista;
        return listModel;
    }
}
