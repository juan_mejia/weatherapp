/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.fcys.yass.wheatherapp;

import net.aksingh.owmjapis.api.APIException;
import net.aksingh.owmjapis.core.OWM;
import net.aksingh.owmjapis.model.CurrentWeather;
import net.aksingh.owmjapis.model.param.Weather;

/**
 *
 * @author UNI
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws APIException {
        // declaring object of "OWM" class
        OWM owm = new OWM("4680ca7dfde895258d64c24fca7265ed");

        // getting current weather data for the "London" city
        CurrentWeather cwd = owm.currentWeatherByCityName("Ticuantepe,NI");
        
        // wind speed
        System.out.println("Wind Speed: " + cwd.getSystemData().getSunriseDateTime());
        
        // wheather
        for(Weather w : cwd.getWeatherList()){
            System.out.println("" + w.getDescription());
        }
        //printing city name from the retrieved data
        System.out.println("City: " + cwd.getCityName());

        // printing the max./min. temperature
        System.out.println("Temperature: " + (cwd.getMainData().getTempMax()-273)
                            + "/" + (cwd.getMainData().getTempMin()-273) + "\'C");
    }
    
}
